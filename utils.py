#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#################################################################################
# Util Module, forcefully inheirted by all modules

import time, hashlib, datetime, uuid, string, secrets
import json, zlib, base64

# Set server debug level
# 0 - Production/Stable, Silent
# 1 - Production/Unstable
# 2 - Development/Debug
debug=1

# Create a variable called "salt", which contains the token saltening sequence
s=open("salt.txt", "r")
for l in s:
    salt=l
s.close()

def ifte(ifs, then, elses):
    if (ifs):
        return then
    else:
        return elses

def md5(string):
    return hashlib.md5(string.encode()).hexdigest()

def md5salt(string):
    global salt
    fs=salt+string
    return hashlib.md5(fs.encode()).hexdigest()

def create_token():
    return uuid.uuid4().hex

def create_password(size=32):
    alphabet = string.ascii_uppercase + string.digits
    return ''.join(secrets.choice(alphabet) for i in range(size))

def now():
    return int(time.time())

# [0]- Day, [1]- Month, [2]- Year, [3]-Weekday, [4]-Hour, [5]-Minute
def return_date(dt):
    day=dt.timetuple()[2]
    month=dt.timetuple()[1]
    year=dt.timetuple()[0]

    hour=dt.timetuple()[3]
    minute=dt.timetuple()[4]

    weekday=dt.timetuple()[6]

    #second=5, days_since_01/01=7
    return day, month, year, weekday, hour, minute

# Date from a SQL DATETIME string
def date_from_sql(date):
    # We assume date was taken from SQL DATETIME field
    dt=datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    return return_date(dt)

# Current date (NOT UTC)
def date_from_now():
    dt=datetime.datetime.now()
    return return_date(dt)


def stdout(mx, verbose=1):
    if (debug >= verbose):
        print(mx)
        dfile=open("debug.txt", 'a')
        dt=datetime.datetime.now().timetuple()
        date="[%04d-%02d-%02d %02d:%02d:%02d]" % (dt[0], dt[1], dt[2], dt[3], dt[4], dt[5])
        dfile.write("%s %s\n" % (date, mx))
        dfile.close()

def compress(string):
    stdout("Compress: Received JSON data: %s" % (string), 1)
    sjs=json.dumps(string)
    sjs=sjs.encode("ascii")
    sjs=zlib.compress(sjs)
    sjs=base64.b64encode(sjs)
    sjs=sjs.decode("ascii")
    stdout("Compress: Sending base64+zlib JSON ASCII data: %s" % (sjs), 2)
    return sjs

# Search for array[?][key]==search in an array of dicts
# Returns the dictionary, or returns "ERROR"
def dl_search(array, key, search):
    return next((item for item in array if item[key] == search), "ERROR")

# Global classes
# We need to override standard list method. Original by Triptych (stackoverflow)
class dlist(list):

    def __setitem__(self, index, value):
        size = len(self)
        if index >= size:
            self.extend(None for _ in range(size, index + 1))

        list.__setitem__(self, index, value)

# Search for a client by user ID
# Returns the dictionary, or returns "ERROR"
def cli_search(search):
    global clients
    return next((item for item in clients if item.userid == search), "ERROR")

# Global structures
Player={}
Battle={}
ApTimer={}
clients = []

# Databases
# Load quest database
f=open("quests.json", "r")
allquests=json.load(f)
f.close()

# Load units database
f=open("units.json", "r")
allunits=json.load(f)
f.close()

# Load skills database
f=open("skills.json", "r")
allskills=json.load(f)
f.close()

# Load summons database
f=open("summons.json", "r")
allsummons=json.load(f)
f.close()

# Load taverns database
# Chance total must be 100% (or 10000). Rolled ONCE
f=open("bar.json", "r")
allbars=json.load(f)
f.close()

###########################################################################
# Comments
"""
abc=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
     'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
     'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
"""

