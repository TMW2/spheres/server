#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#################################################################################
# Protocol Module
# Parses packets
import utils
import player
import tavern
import battle.main as battle
import battle.summons as summon
from consts import ERR_BAD, ERR_DONE, PACKET_NACK, PACKET_ACK, PACKET_REGX

# self.data structure: <Token> ; <Message> ; <JSON Arguments>
def parse(packet, conn):
    ##########################################################################
    # Maybe it is a DEBUG packet
    if (packet.lower() == "ping"):
        return [PACKET_ACK, "PONG"]

    ##########################################################################
    # Not a debug packet, so it must adhere to the convention
    data=packet.split(';')
    if (len(data) != 3):
        return [PACKET_NACK, "Invalid packet (RAW)"]
    t=data[0]

    ##########################################################################
    #  Maybe it is a raw packet which doesn't requires auth
    if (data[1] == "PING"):
        return [PACKET_ACK, "PONG"]

    ##########################################################################
    # Maybe it is an honest login attempt, or a registration attempt
    if data[1] == "login":
        # Create a token
        if t == "0":
            t=utils.create_token()
            conn.token = str(t)

        r=player.get_data(data[2], t)
        if r is ERR_BAD:
            return [PACKET_NACK, "Login error"]
        elif type(r) is int:
            return [PACKET_ACK, r] # FIXME: Lie?
        else:
            conn.userid=int(utils.Player[conn.token]["userid"])
            conn.MS_auth=True
            utils.stdout("Login successful for UID %d" % conn.userid)
            return [PACKET_ACK, r]
    elif data[1] == "register":
        r=player.register(data[2], t)
        if r is ERR_BAD:
            return [PACKET_REGX, "Invalid registration attempt"]
        else:
            return [PACKET_ACK, r]

    ##########################################################################
    # It is not, so Validate token format
    print("Data0: %s | Alpnum: %s" % (str(data[0]), str(data[0].isalnum()) ))
    if not data[0].isalnum():
        return [PACKET_NACK, "Invalid packet (TOKENF)"]

    # Now validate the token itself
    if data[0] != conn.token:
        return [PACKET_NACK, "Invalid packet (TOKENC)"]

    ##########################################################################
    # Top level protocols
    if data[1] == "logout":
        player.clear(t)
        r=ERR_DONE
    elif data[1] == "get_inv":
        r=player.get_inv(data[2], t)
    elif data[1] == "apdata":
        r=player.ap_data(data[2], t)
    elif data[1] == "battle":
        r=battle.battle(data[2], t)
    elif data[1] == "reload_battle":
        try:
            r=battle.reload_battle(t)
        except:
            r=[PACKET_ACK, "CANCELLED"]
    elif data[1] == "begin_quest":
        r=battle.begin_quest(data[2], t)
    elif data[1] == "get_party":
        r=player.get_party(data[2], t)
    elif data[1] == "set_party":
        r=player.set_party(data[2], t)
    elif data[1] == "upgrade":
        r=player.upgrade(data[2], t);
    elif data[1] == "evolve":
        r=player.evolve(data[2], t);
    elif data[1] == "sellunits":
        r=player.sellunits(data[2], t);
    elif data[1] == "recruit":
        r=tavern.recruit(data[2], t)
    elif data[1] == "summon":
        r=summon.summon(data[2], t)
    else:
        r=ERR_BAD

    if r == ERR_BAD:
        return [PACKET_NACK, "Invalid Protocol (400)"]
    else:
        return [PACKET_ACK, r]

