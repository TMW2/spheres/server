#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#################################################################################
# Constants Module

# Elements
Ele_Fire    =1
Ele_Water   =2
Ele_Nature  =3
Ele_Light   =4
Ele_Shadow  =5

# Gender
Sex_Male    =0
Sex_Female  =1

# Limits
NO_LVLUP    =1
NO_PARTY    =2
EXP_UP      =4
EVO_MAT     =8
DOUBLE_GP   =16
UNSELLABLE  =32
SUPEREVOMAT =64

# Status
ST_TOWN     =0
ST_QUEST    =1

# Spheres
SPH_NONE        =0
SPH_WIDEATTACK  =1
SPH_PIERCE      =2
SPH_ASSAULT     =3
SPH_HEAL        =4
SPH_HEALALL     =5
SPH_ATKUP       =6
SPH_DEFUP       =7

# Combat code
SRV_SKILL       =1000
SRV_WAVE        =9901
SRV_SPHERE      =9902
SRV_SUMMON      =9903
SRV_NOCAST      =9904

# Status Conditions
SC_ATKUP        =1
SC_DEFUP        =2
SC_ATKDOWN      =4
SC_DEFDOWN      =8
SC_POISON       =16
SC_BLIND        =32
SC_STONED       =64

# Battle action
ACT_NONE        =0
ACT_SPHERE      =1
ACT_SKILL       =2

# Date Constants
# [0]- Day, [1]- Month, [2]- Year, [3]-Hour, [4]-Minute, [5]-Weekday
DATE_DAY        =0
DATE_MONTH      =1
DATE_YEAR       =2
DATE_WEEKDAY    =3
DATE_HOUR       =4
DATE_MINUTE     =5

MONDAY          =0
TUESDAY         =1
WEDNESDAY       =2
THURSDAY        =3
FRIDAY          =4
SATURDAY        =5
SUNDAY          =6

# Configuration (hardcoded!)
MAX_INV_SIZE    =100
MAX_PARTIES     =3
AP_REGEN_TIME   =360
AP_REGEN_TIME_F =360.0
SQL_SAVE_TIME   =300.0
CONN_LIFETIME   =1800
CONN_CLEANUP    =900.0
SQL_PINGTIME    =1200.0
BL_UPDATETIME   =30.0
BAN_TIME        =180.0
BAN_UNAUTHED    =5
BAN_AUTHED      =30
CLIENTVERSION   ="2.2.7.27"
MAX_CLIENTS     =2500

# Hard coded loot (1,000~10,000)
CRYSTAL_MIN=1000
CRYSTAL_MAX=2000
EXPRATE_MIN=2001
EXPRATE_MAX=4000
GPPRATE_MIN=4001
GPPRATE_MAX=6000

# Quick shortcuts
CRYSTAL_10 =1010
CRYSTAL_20 =1020
CRYSTAL_30 =1030
CRYSTAL_50 =1050
CRYSTAL_100=1100
DOUBLE_EXP =3000
TRIPLE_EXP =4000
DOUBLE_GP  =5000
TRIPLE_GP  =6000

# Error Handler
ERR_ERR     ="500 Internal Server Error" # Maybe 418 I am a teapot
ERR_OFF     =401
ERR_BAD     ="400 Bad Request"
ERR_DONE    ="200"
#"You cannot perform this operation! Not enough Crystals"
ERR_INS     =105
ERR_FULL    =106
ERR_SUMMON  =108
#"Operation complete"
ERR_OK      =200
ERR_LOGIN   =5000

# Packet result
PACKET_REGX =-1
PACKET_NACK =0
PACKET_ACK  =1

# SQL masks
SQL_NONE    =0
SQL_CLEAR   =1
SQL_DELAY   =2
SQL_FULL    =4

# quest.json flags
SFLAG_NONE          =0 # Normal battle
SFLAG_CLEARGEMS     =1 # Give 100 gems upon first victory
SFLAG_DOUBLEGEMS    =2 # Give 2x crystals (200 gems) upon first victory
SFLAG_SPECIAL       =4 # This is a special quest - do not update quest field
SFLAG_FIRSTLOOT     =8 # Always collect the first loot on first victory.
SFLAG_DOUBLEEXP     =16 # Double EXP earned
SFLAG_DOUBLEGP      =32 # Double GP earned

# Skill types
SK_CLEAR_SC         =1
SK_SINGLE_DMG       =2
SK_MULTI_DMG        =4
SK_ATK_UP           =8
SK_DEF_UP           =16
SK_SINGLE_HEAL      =32
SK_MULTI_HEAL       =64
SK_RESSURECTION     =128

# Others
INT_MAX             =2147483647

#################################################################################
EXPTABLE = [0, 158, 318, 480, 644, 809, 976, 1145, 1315, 1487, 1661, 1836, 2014, 2192, 2373, 2555, 2739, 2925, 3112, 3301, 3492, 3684, 3878, 4074, 4271, 4470, 4671, 4874, 5078, 5284, 5491, 5701, 5912, 6124, 6339, 6555, 6772, 6992, 7213, 7436, 7660, 7886, 8114, 8344, 8575, 8808, 9043, 9279, 9517, 9757, 9999, 10242, 10487, 10733, 10981, 11231, 11483, 11736, 11991, 12248, 12506, 12767, 13028, 13292, 13557, 13824, 14092, 14363, 14635, 14908, 15184, 15461, 15740, 16020, 16302, 16586, 16872, 17159, 17448, 17738, 18031, 18325, 18621, 18918, 19217, 19518, 19820, 20125, 20431, 20738, 21048, 21359, 21671, 21986, 22302, 22620, 22939, 23260, 23583, 23908, 24234]

