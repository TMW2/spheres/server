########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
########################################################################################
# Tavern Recruitment Module

from utils import (stdout, compress, allbars, Player)
from consts import (ERR_OK, ERR_BAD, ERR_INS, ERR_FULL, ST_TOWN)
from player import inventoryplace
import random, json

def recruit(args, token):
    # we have: args["tavern"] and args["amount"], besides Player[token]
    try:
        stdout("Recruit: %s" % args)
        tv=json.loads(args)
        stdout("JSON loaded")
        # Validation
        tmp=int(tv["amount"])
        tmp=int(tv["tavern"])
        # Load requested tavern
        tavern=allbars[tmp]
        if tavern == "ERROR":
            stdout("Request for inexistent tavern id %d" % tmp)
            raise Exception("Tavern not found")
        # Tavern must be active
        if tavern["min_quest"] < 0:
            stdout("Request for disabled tavern id %d" % tmp)
            raise Exception("Tavern is not active")
        # Tavern must be unlocked
        if tavern["min_quest"] > Player[token]["quest"]:
            stdout("Request for locked tavern id %d" % tmp)
            raise Exception("Tavern is not active")
        # Currency must exist and be valid (actually our fault...)
        tmp=int(Player[token][tavern["currency"]])
    except:
        # Invalid data
        return ERR_BAD

    # Configure tavern settings
    price=tavern["price"]
    poll=tavern["tavern_list"]

    # Check if you can recruit right now
    if (Player[token]["status"] != ST_TOWN):
        return ERR_BAD

    # Where we'll be saving your acquisitions
    Result={"units": []}
    Rarity=[] # <-- Internal control only

    # Main Loop
    i=0
    while i < tv["amount"]:
        i+=1
        # Not enough crystals/currency
        if (Player[token][tavern["currency"]] < price):
            Result["code"]=ERR_INS
            break

        # Inventory is full (also, get IDX)
        idx=inventoryplace(token)
        if (idx == -1):
            Result["code"]=ERR_FULL
            break

        # Payment in specified currency... (Defaults to crystals)
        Player[token][tavern["currency"]]-=price

        # Grab a random entry per groups
        r=random.randint(0, 10000)
        o=0
        for group in poll:
            if (r < group["chance"]):
                o=random.choice(group["units"])
                break
            else:
                # Adjust random seed so it is consistent
                r-=group["chance"]

        # Maybe, just maybe, you have a warranted max-rarity card
        if i % tavern["min_draws"] == 0:
            ssr=0
            for ur in Rarity:
                if ur in poll[0]["units"]:
                    ssr+=1
            if ssr < i / tavern["min_draws"]:
                o=random.choice(poll[0]["units"])

        # We don't want base id, we need unit id
        o*=100

        # Now we need to add it in a structured form
        unit={
            "unit_id": o,
            "level": 1,
            "exp": 0
        }

        # Record the new unit
        Result["units"].append(unit)
        try:
            Player[token]["inv"][idx]=unit
        except:
            Player[token]["inv"].append(unit)
            print("\033[31mAn error happened in recruit (token: %s idx: %d unit: %s InvLen: %d)\033[0m" % (token, idx, str(unit), len(Player[token]["inv"])))

        # We want to update Rarity Control
        Rarity.append(o)

    # Feed back the currency data
    Result["currency"]=(tavern["currency"], Player[token][tavern["currency"]])

    # Shuffle Results to look more random
    random.shuffle(Result["units"])

    try:
        stdout("Response: %d " % Result["code"])
    except:
        Result["code"]=ERR_OK

    sjson=compress(Result)
    #stdout("Reply: %s" % sjson)
    return sjson


