CREATE DATABASE database;
CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON `database`.* TO 'username'@'localhost';
FLUSH PRIVILEGES;
