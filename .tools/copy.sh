#!/bin/bash
DEST="../htdocs/spheres/"
mkdir -p $DEST
echo "Copying web files to $DEST (the folder will be created if needed)"
cp quests.json $DEST
cp story.json $DEST
cp units.json $DEST
cp version.txt $DEST
#cp certificate.pem $DEST
cp news.json $DEST
cp bar.json $DEST
cp world.json $DEST
cp summons.json $DEST
cp skills.json $DEST # Not yet used
echo "Done! Move them to your webserver folder"

