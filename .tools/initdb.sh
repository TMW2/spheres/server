#!/bin/bash
echo Creating db and user...
echo Enter mysql root password:
mysql --force -u root -p < .tools/init.sql
result=$?
if [ "$result" != 0 ]; then
	echo Password failed. Trying with sudo...
	sudo mysql --force -u root -p < .tools/init.sql
fi
# While on that, we should generate a salt. Just a "random" one.
# This is the default value, YOU SHOULD CHANGE IT.
# A stronger salt could be generate with `date +%s|md5sum|base64`
# But salt is really internal, a single unguessable char is enough.
# KEEP IT SMALL.
echo $RANDOM % 1000 > salt.txt
echo "Salt created (see: salt.txt)"
echo "Creating PEM certificate for SSL connections..."
openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem
echo "Remember to copy certificate.pem file to your webserver."
echo "Otherwise, you'll need to disable SSL support on pass.json config."
#echo "Or run the server without SSL protocol: ./server.py --no-ssl"

# Misc setup
cp news.json.example news.json
cp pass.json.example pass.json
touch debug.txt
date +%s > version.txt

echo "DB, SALT and PEM creation complete!"

wget -t 5 -c https://lists.blocklist.de/lists/all.txt -O Z-Line.txt

echo "Z-Line populated with blocklist.de data"
