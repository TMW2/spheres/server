# Installing

Make sure you have the following dependencies:

Python3 Modules:
random, threading, time, hashlib, json, zlib, base64, mysql.connector, syslog

You need Python 3.6, other versions were not tested and may not work at all.
You'll also need a local instance of MySQL running. MariaDB should also work.

mysql.connector can be installed with `pip3 install mysql-connector`.

The websocket library is available [here](https://pypi.org/project/simple-websocket-server/) and **comes pre-installed**, no need to use pip.

## Before all

Create a database, SQL User and SQL Password, granting all rights to it.
Remember to configure pass.json accordingly.

Then, execute `main.sql` against the newly created SQL Database.
It's installed now.

### Alternative
You can also run this command to prepare MySQL for you. You need to have mysql
installed to run this command!

```sh
make initdb
```

Do note it'll try to create "username", identified by "password", owner of
"database" in your SQL, and it might mess up badly, but should do for a test.

## Other things you'll need

You need a file called salt.txt, with 1~5 random alphanumeric characters.
If you used `make initdb`, this file was created to you with a random 0~999 number.
You probably should change it, but it is fine as long as it can't be guessed.

If you plan in support SSL, you will need a pair of files: key.pem and certificate.pem
`make initdb` will make these files for you; But if you don't want to support SSL,
update the relevant setting on server.py file.

Remember: Clients may attempt SSL connection if they can download certificate.pem!

You should also set up the update server if you have not done so already.
(It is a different repository).

# Running the server
Simply run server.py and vóila.

Spheres Server will be running on port provided by pass.json
Remember to configure server.py if you plan on running this outside localhost.

# Securing the server

You should, besides changing default sql user/password combination, use Fail2Ban.
Fail2Ban will block harmful clients which could otherwise DoS your server.

See also their official website: https://www.fail2ban.org

Major auth failures will be sent to syslog.

The server will read (one IP per line) the files called Z-Line, G-Line and K-Line
in this order during startup, and won't read them again at runtime.
They will issue "bans", which causes connection to be dropped right after being
established. For several reasons, you should deny problematic IPs **before** the
connection is established, not right after; So do not understimate a properly
configured fail2ban.

The files are not distinguished among themselves, however, Z-Line.txt will be
populated with `make initdb` with blocklist.de recommendations, and K-Line can be
populated by using the `kline` server console command. G-Line remains available
for any extra ban list you might have or wish - for example, you could download
[Tor Exit Nodes List](https://check.torproject.org/torbulkexitlist?ip=1.1.1.1)
and write G-Line.txt with it to prevent access to the game from Tor.

Keep in mind that `kline` console command will write to K-Line.txt so
autogenerating data for it is not advised. All bans expire when server restarts,
except if they have been kline'd (or are otherwise listed on a -Line file.)

Do note it do not support zones (eg. /24) nor does it support wildcards. Invalid
lines will be stored to memory but will never trigger the ruleset. Thus the advise
for an external properly configured firewall, the built-in measures are minimal,
and just to act as a _last_ defense line against intruders - not an _only_.

Other suggestions (never tested):
* [Blocklist DE](blocklist.de) - IP-Addresses who attack other servers/honeypots over SSH, FTP, IMAP, etc.
* [SORBS NET](sorbs.net) - Open SOCKS proxy servers, etc
* [Spamhaus ORG](spamhaus.org) - Spamhaus blacklist (spammers, open proxies)
* [Proxy-List DOWNLOAD](www.proxy-list.download/) - List of SOCKS and HTTP proxies

## Automatic bans

The server has a "score" function in security. Sending invalid packets will cause
your score to raise, and once it reaches a certain threshold, the connection will
be killed and the user IP will be banned for a short while (BAN_TIME in consts.py)

Different errors might influence the score differently depending on the severity.
The threshold fluctuates depending if the user is logged in or not - for instance,
sending a packet while logged out will most likely trigger the ban rules.

For this reason, server admins are advised to tweak the values to their liking.
The values are in consts.py and not in configure. Changing the score for each
invalid operation must be done at the python file of the operations specifically.

# The client
The client should work out-of-the-box, but a few concerns are to be made.

1. Only localhost is accepted right now,
2. You must serve several files under localhost:80, eg.
  * version.txt
  * units.json
  * quests.json
3. You must have Update Server running and properly configured,

Remember to run the server along Fail2ban and other technologies.

Client auto-updater is not provided, but is possible.

# The Update Server

The client will request images missing on it to the update server.

It should be served at 443 port of the main host, under the assets/ folder.
HTTPS support is mandatory, but the certificate is not validated.

The user token will also be sent as a GET parameter on every request. While the
update server may safely ignore this parameter, verifying the token allows you to
limit who can download the data to only authenticated users. The benefits of doing
this are still not proven.

If you use Apache or Nginx, remember to disable directory listing.

Also: We advise serving your images in the WebP format. Extension should not be
provided by the webserver.

# Limitations

This software can reliably send 4091 bytes at once. The biggest payload ever
tested was a Lorem Ipsum with about 20,000 bytes, which was delivered correctly.
Even so, we do not advise going past the 4091 bytes.

# The Control Panel

After the server start, it will listen for a few commands. They are listed below:

| Command | Aliases | Effects |
| ------- | ------- | ------- |
| broadcast | global, msg, say, kami | Sends a plain text notice to all connected clients (regardless of auth state). Useful to warn about maintenances. |
| kick | dc | Disconnects an IP from the server. |
| ban | nuke, kb | Bans an IP until the server restarts. |
| unban |  | Unbans an IP until the server restarts. |
| kline | permaban, block | Bans an IP and record it on K-Line.txt - Survives server restarts, is permanent |
| exit | quit, close, term, end | Sends a shutdown notice to all clients and shuts down the server. | 
| debug | dbg | Toggles server debug mode on and/or off. |
| raw | eval | Evaluates an arbitrary Python statement if server is in debug mode. |
| run | exec | Executes an arbitrary Python statement if server is in debug mode. |
| status | st | Reports status regarding server health |
| list | all | Reports data on all connected clients |
| ddos | dcall | Disconnects all unauthed clients. |
| ddosban | dcbanall | Disconnects all unauthed clients, and ban their IP for half hour. |

Keep in mind that running arbitrary python code may be dangerous.
Be sure you understand the risks before using `RAW` and `RUN` commands.

The commands themselves are case-insensitive, but the arguments are
case-sensitive. It is expected to the commands be properly formatted, failure to
do so may thrown unhandled exceptions and cause the server to perform an
emergential exit.
