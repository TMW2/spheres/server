#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019-2021  Jesusalva

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#################################################################################
# Battle Module - Skills
# SRV_NOCAST => Effects of SRV_SKILL follows
from utils import stdout, Battle
from consts import (SK_SINGLE_DMG, SK_MULTI_DMG, SK_ATK_UP, SK_DEF_UP,
    SK_SINGLE_HEAL, SK_MULTI_HEAL, SC_ATKUP, SC_DEFUP,
    SRV_SKILL, SRV_NOCAST)
from battle.common import find_target, calc_dmg, attackall
def skill_core(token, mask, force, element):
    global Battle

    # Create a fake unit
    dummy={
            "name": "Dummy",
            "unit_id": 1,
            "max_hp": 1,
            "hp": 1,
            "atk": force,
            "ele": element,
            "status_effects": 0
            }

    # Handle the mask to determine the action
    # TODO: SK_CLEAR_SC, SK_RESSURECTION
    if mask & SK_SINGLE_DMG:
        # Single Damage skill: 1 strength = 1 damage
        target_id=find_target(token, "enemy")
        stdout("Enemy selected: %d" % target_id)
        target=Battle[token]["enemy"][target_id]
        dmg = calc_dmg(token, dummy, target, dummy["atk"])
        target["hp"] -= dmg
        Battle[token]["log"].append(["", 0, SRV_NOCAST, dmg, "enemy", target_id])

    if mask & SK_MULTI_DMG:
        # Multi Damage skill: 1 strength = 1 damage
        attackall(token, dummy, "enemy")

    if mask & SK_ATK_UP:
        # Attack up skill: Add SC_ATKUP to all members (even dead ones)
        for target in Battle[token]["party"]:
            if not (target["status_effects"] & SC_ATKUP):
                target["status_effects"]=target["status_effects"]|SC_ATKUP

    if mask & SK_DEF_UP:
        # Defense up skill: Add SC_DEFUP to all members (even dead ones)
        for target in Battle[token]["party"]:
            if not (target["status_effects"] & SC_DEFUP):
                target["status_effects"]=target["status_effects"]|SC_DEFUP

    # TODO: Is single heal the same as self heal?
    if mask & SK_SINGLE_HEAL:
        # Heal skill: 1 strength = 1% healing (FIXME?)
        target_id=find_target(token, "party")
        target=Battle[token]["party"][target_id]
        target["hp"]+=int(target["max_hp"]*dummy["atk"]/100)
        if (target["hp"] > target["max_hp"]):
            target["hp"]=0+target["max_hp"]

    if mask & SK_MULTI_HEAL:
        # Heal skill: 1 strength = 1 HP (FIXME?)
        dummy["atk"]*=-1
        attackall(token, dummy, "party")

    # Victory/Defeat checks are not handled here
    # This doesn't returns anything, actually.
    # Errors must be handled as Exceptions
    return

def handle_skill(token, skill_id):
    global Battle
    # TODO / FIXME
    dmg = 0
    Battle[token]["log"].append(["who", 0, SRV_SKILL+skill_id, dmg, "target", 0])
    return


#################################################
# Handles CI false positives in a lame way
def pyflakes():
    return Battle

