#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019-2021  Jesusalva

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#################################################################################
# Battle Module - Summons
import json, traceback
from utils import (compress, allsummons, stdout,
    Player, Battle)
from consts import (ERR_ERR, ERR_SUMMON, SRV_SUMMON)
from battle.skills import skill_core
from battle.common import conditions
#from battle.main import advance_wave, battle_endturn, get_result

def handle_summon(token, summon, summon_id):
    global Battle, Player

    try:
        # Log that summon was used (single action log)
        Battle[token]["log"]=[["", 0, SRV_SUMMON, summon_id, "", 0]]

        # Summon strength is based on player rank
        force=summon["strength"]*Player[token]["level"]

        # Cast the skill, return error 500 on exception
        skill_core(token, summon["type"], force, summon["attribute"])
    except:
        traceback.print_exc()
        return ERR_ERR

    stdout("Summon: Victory/Defeat Check", 2)

    # HOLD THAT! Handle victory/defeat conditions
    check = conditions(token, Battle[token]["spheres"])
    if check is not None:
        return check

    # The check is: if not ERR_ERR or ERR_BAD, then reload battle/result
    sjson=compress(Battle[token])
    # We should also clear the log - again
    Battle[token]["log"] = []
    return sjson


def summon(args, token):
    # Data validation
    try:
        stdout("Summon: %s" % args)
        ss=json.loads(args)
        # [summon_id]
        # Validation
        summon_id=int(ss[0])
        assert summon_id > 0, "Summon ID understated"
        assert summon_id < 127, "Summon ID overstated"

        # Create summon object
        summon=allsummons[summon_id]

        # Verify the lock
        if (Player[token]["max_sum"] < summon_id):
            stdout("Cannot summon \"%s\": Not unlocked" % (summon["name"]), 2)
            raise Exception("Locked summon")

        # Verify the cost
        if Battle[token]["bp"] < summon["cost"]:
            stdout("Cannot summon \"%s\": Insufficient BP (%d/%d)" % (summon["name"], Battle[token]["bp"], summon["cost"]), 2)
            raise Exception("Insufficient BP")

        stdout("All fine thus far")
    except:
        # Invalid data
        return compress(ERR_SUMMON)

    # Already summoned (reverse logic)
    try:
        Battle["s"]+=1
        Battle["s"]-=1
        return compress(ERR_SUMMON)
    except:
        pass

    Battle[token]["bp"]-=summon["cost"]
    Battle["s"]=1
    return handle_summon(token, summon, summon_id)


#################################################
# Handles CI false positives in a lame way
def pyflakes():
    return Player

