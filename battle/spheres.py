########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
########################################################################################
# Battle Module - Spheres
from utils import stdout, Battle
from consts import (SPH_WIDEATTACK, SPH_PIERCE, SPH_ASSAULT, SPH_HEAL, SPH_HEALALL,
    SPH_NONE, SPH_ATKUP, SPH_DEFUP, SC_ATKUP, SC_DEFUP)
from battle.common import find_target, attackall, calc_dmg

# Handle sphere attacks. Unit is a member from Battle[token][scope]
def sphere_attack(token, unit, sphere, idx=0):
    stdout("%s is attacking" % str(unit), 2)
    stdout("Sphere selected: %d" % sphere, 2)
    dmg = 0

    # Select the enemy which should be attacked
    target_id=find_target(token, "enemy")
    stdout("Enemy selected: %d" % target_id, 2)
    target=Battle[token]["enemy"][target_id]

    # Now cycle though spheres
    if (sphere == SPH_WIDEATTACK):
        # Wide Attack sphere
        attackall(token, unit, "enemy")
    elif (sphere == SPH_PIERCE):
        # Pierce sphere (100% critical chance)
        dmg = calc_dmg(token, unit, target, unit["atk"], 1.0)
        target["hp"]-=dmg
    elif (sphere == SPH_ASSAULT):
        # Assault sphere (50% critical chance)
        dmg = calc_dmg(token, unit, target, unit["atk"], 0.5)
        target["hp"]-=dmg
    elif (sphere == SPH_HEAL):
        # Heal sphere (30% healing). Need to redefine target
        target_id=find_target(token, "party")
        target=Battle[token]["party"][target_id]
        dmg=-int(target["max_hp"]*0.3)
        target["hp"]-=dmg
        if (target["hp"] > target["max_hp"]):
            target["hp"]=0+target["max_hp"]
    elif (sphere == SPH_HEALALL):
        # Heal All (negative damage to party)
        unit["atk"]*=-1
        attackall(token, unit, "party")
        unit["atk"]*=-1
    elif (sphere == SPH_NONE):
        # Normal Attack
        stdout("It's a normal attack!", 2)
        dmg=calc_dmg(token, unit, target, unit["atk"])
        target["hp"]-=dmg
    elif (sphere == SPH_ATKUP):
        # Attack up sphere: Add SC_ATKUP to all members (even dead ones)
        for target in Battle[token]["party"]:
            if not (target["status_effects"] & SC_ATKUP):
                target["status_effects"]=target["status_effects"]|SC_ATKUP
    elif (sphere == SPH_DEFUP):
        # Defense up sphere: Add SC_DEFUP to all members (even dead ones)
        for target in Battle[token]["party"]:
            if not (target["status_effects"] & SC_DEFUP):
                target["status_effects"]=target["status_effects"]|SC_DEFUP
    else:
        # Something went wrong, report and do nothing
        print ("\033[1;31mERROR, INVALID SPHERE ID: %d\033[0m\n" % sphere)
        return False

    # We are done!
    #stdout("Attack completed")
    ## FIXME
    if sphere not in [SPH_HEAL, SPH_HEALALL]:
     Battle[token]["log"].append(["party", idx, sphere, dmg, "enemy", target_id])
    elif sphere == SPH_HEAL:
     Battle[token]["log"].append(["party", idx, sphere, dmg, "party", target_id])
    return True

