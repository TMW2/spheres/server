all:
	@echo "Current makes:"
	@echo "initdb      - initializes a MySQL database"
	@echo "sql         - Update MySQL database"
	@echo "web         - Copies all web files to ../htdocs/spheres"
	@echo "update      - Deploys a new version using make web"

.PHONY: initdb
initdb:
	.tools/initdb.sh
	make sql

.PHONY: sql
sql:
	@echo "Populating database"
	@mysql -u username -ppassword database < main.sql
	@echo "MySQL is now ready to use"

.PHONY: web
web:
	.tools/copy.sh

.PHONY: update
update:
	date +%s > version.txt
	@echo "New version deployed: "
	@cat version.txt
	make web

